#ifndef USERPROG_H
#define USERPROG_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define PROGRAM_NAME 		(printf("\t\t\t\t======== Phone Book ========\n"))
#define NB_CHARACTER 		(100)
#define MAX_CONTACT 		(10000)


/* structure for informations of a contact */
typedef struct  
{
	int  contactId;
	char name[NB_CHARACTER];
	char phone[NB_CHARACTER];
	char email[NB_CHARACTER];
	char street[NB_CHARACTER];
	int  postalCode;
	char town[NB_CHARACTER];
}CONTACT_INFO_t;



/* Functions prototype */
void menu();
void inputOption(int *const option);
void defaultEnterOptionCase();
void exitApp();
void loadingBar(int const durationInSec);		
void flushBuffer();
void clearScreen();
void loadContactsFromDatabase(CONTACT_INFO_t *ptr);
void saveContactsInDatabase(CONTACT_INFO_t *ptr);
void initialization(CONTACT_INFO_t *const pBase);
void displayPhoneBook(CONTACT_INFO_t *const pBase);
void readString(char *string, int nbReadChar);
void inputInterger(int *pt, char message[NB_CHARACTER]);
void isContactExist(CONTACT_INFO_t *const pBase, int const inputID, int *contactExist, int *indContact);
void addNewContact(CONTACT_INFO_t *const pBase);
void editContact(CONTACT_INFO_t *const pBase);
void deleteAContact();
void deleteAllContact(CONTACT_INFO_t *const pBase);


/* Functions definition */

/* display menu */
void menu()
{                  								     
	printf("\t\t\t\t/---------------------------\\\n");
	printf("\t\t\t\t:  1- Display all contacts  :\n");
	printf("\t\t\t\t:  2- Add new contact       :\n");
	printf("\t\t\t\t:  3- Edit a contact        :\n");
	printf("\t\t\t\t:  4- Delete a contact      :\n");
	printf("\t\t\t\t:  5- Delete all contacts   :\n");
	printf("\t\t\t\t:  ");
	printf("\033[1;31m"); //Set the text to the color red and bold 
	printf("0- Quit");
	printf("\033[0m");    //Resets the text to default color
	printf("\t\t    :\n");
	printf("\t\t\t\t\\---------------------------/\n");
}

/* ask the user choose option */
void inputOption(int *const option)
{
	printf("Enter your option here: ");
	scanf("%d", option);
}

void defaultEnterOptionCase()
{
	printf("\033[1;31m"); //Set the text to the color red
	printf("ERROR : Invalid Enter\n"); 
	printf("\033[0m");    //Resets the text to default color
	flushBuffer();
	printf("Press enter key to clean the screen...");
	getchar();
	system("clear");
}

/* exiting app */
void exitApp()
{
	system("printf \"App exiting in %s second(s) | \" \"5\" ");
	loadingBar(5);
	printf("App exited successfully\n");
}

/* loading bar */
void loadingBar(int const durationInSec)
{
	for(int i = 0; i < durationInSec; i++)
	{
		system("printf \"#\"");
		sleep(1);
	}
	printf("\n");
}

/* flush the buffer */
void flushBuffer()
{
	int c = 0;
	while(c != '\n' && c != EOF)
	{
		c = getchar();
	}
}

/* clear the screen */
void clearScreen()
{
	printf("Press enter to clean the screen...");
	flushBuffer();
	system("clear");
}


/* load contacts from dedicated database */
void loadContactsFromDatabase(CONTACT_INFO_t *ptr)
{
	FILE *file = NULL;
	file = fopen("database.txt", "rb");  // open database 

	if(file == NULL) // if an openiing error occurs 
	{
		perror("Opening error");
		//system("touch database.txt");  // create a file name database.txt
	}
	else
	{
		fread(ptr, sizeof(ptr), MAX_CONTACT, file);
	}
	fclose(file);
	printf("Up-to-date Phone Book\n");
}

/* write contacts in dedicated database */
void saveContactsInDatabase(CONTACT_INFO_t *ptr)
{
	FILE *file = NULL;
	file = fopen("database.txt", "wb+");  // open database 

	if(file == NULL) // if an openiing error occurs 
	{
		perror("Opening error");
	}
	else
	{
		fwrite(ptr, sizeof(ptr), MAX_CONTACT, file);
	}
	fclose(file);
}

/* initialize all contacts */
void initialization(CONTACT_INFO_t *const pBase)
{
	for(int i = 0; i < MAX_CONTACT; i++)
	{
		pBase[i].contactId 			= 0; 
		strcpy(pBase[i].name,		"");
		strcpy(pBase[i].phone,		"");
		strcpy(pBase[i].email,		"");
		strcpy(pBase[i].street,	    "");
		pBase[i].postalCode 		= 0;
		strcpy(pBase[i].town,	    "");
	}
}

/* display all contacts */
void displayPhoneBook(CONTACT_INFO_t *const pBase)
{
	int nbEmptyContact = 0;
	for(int i = 0; i < MAX_CONTACT; i++)
	{
		if(pBase[i].contactId == 0)
		{
			nbEmptyContact++;
		}
		else
		{
			printf("-----------------------------------------------------------\n");
			printf("\033[1m");
			printf("CONTACT %d\n", 			pBase[i].contactId);
			printf("\033[0m");
			printf("Name: %s\n",    		pBase[i].name);
			printf("Phone: %s\n",   		pBase[i].phone);	
			printf("Email: %s\n",   		pBase[i].email);
			printf("Address: %s, %d %s\n",  pBase[i].street, 
											pBase[i].postalCode,
											pBase[i].town);	
			printf("-----------------------------------------------------------\n");
		}
	}

	if(nbEmptyContact == MAX_CONTACT)
	{
		printf("\033[1;31m"); //Set the text to the color red and bold 
		printf("WARNING : No contat to display.\n"); 
		printf("\033[0m");    //Resets the text to default color
	}

}

/* read string without \n character using fgets */
void readString(char *string, int nbReadChar)
{
	char *returnKeyPosition = NULL;

	if(fgets(string, nbReadChar, stdin) != NULL)
	{
		returnKeyPosition = strchr(string, '\n'); /* search for return key (\n)*/
		if(returnKeyPosition != NULL)
		{
			*returnKeyPosition = '\0'; /* replace \n by \0 <=> remove \n */
		}
		else
		{
			// printf("\\n not present\n");
			flushBuffer();
		}
	}
	else
	{
		printf("\033[1;31m"); //Set the text to the color red and bold 
		printf("ERROR: Can't read the input\n"); 
		printf("\033[0m");    //Resets the text to default color
	}
}

/* check an input integer */
void inputInterger(int *pt, char message[NB_CHARACTER])
{
	int scanfReturn = 0;

	do
	{
		printf("%s ", message);
		scanfReturn = scanf("%d", pt);
		flushBuffer();
		if(scanfReturn == 0)
		{
			printf("\033[1;31m"); //Set the text to the color red and bold 
			printf("ERROR: Invalid input. Please input an integer\n"); 
			printf("\033[0m");    //Resets the text to default color
		}
	}while(scanfReturn == 0);
}

/* check if a contact exists by checking is id */
void isContactExist(CONTACT_INFO_t *const pBase, int const inputID, int *contactExist, int *indContact)
{
	for(int i = 0; i < MAX_CONTACT; i++)			/* write this as a function */
	{
		if(pBase[i].contactId == inputID)
		{
			*contactExist = 1;
			*indContact = i;
			break; 
		}
	}
}


/* add a new contact */
void addNewContact(CONTACT_INFO_t *const pBase)
{
	int isEmptyNode = 0;
	int  indEmptyNode = 0;

	for (int i = 0; i < MAX_CONTACT; i++)   /* write this as a function */
	{
		/* search for an empty node */
		if(pBase[i].contactId == 0)
		{
			isEmptyNode = 1;
			indEmptyNode = i; 
			break; 
		}
	}

	if(!isEmptyNode)
	{
		printf("\033[1;31m"); //Set the text to the color red and bold 
		printf("WARNING: Phone Book is full. Please delete a contact.\n"); 
		printf("\033[0m");    //Resets the text to default color
	}
	else
	{
		/* adding the new contact */
		printf("Input Contact Informations:\n");

		
		int inputContactID = 0;
		int isExist = 0;
		int index = 0;

		inputInterger(&inputContactID, "ID:");

		isContactExist(pBase, inputContactID, &isExist, &index);

		if(isExist) // if the contact exist 
		{
			printf("\033[1;31m"); //Set the text to the color red and bold 
			printf("ERROR: Sorry, this contact already exists.\n"); 
			printf("\033[0m");    //Resets the text to default color
		}
		else
		{
			pBase[indEmptyNode].contactId = inputContactID;

			printf("NAME: ");
			readString(pBase[indEmptyNode].name, NB_CHARACTER);

			printf("PHONE: ");
			readString(pBase[indEmptyNode].phone, NB_CHARACTER);

			printf("EMAIL: ");
			readString(pBase[indEmptyNode].email, NB_CHARACTER);

			printf("STREET: ");
			readString(pBase[indEmptyNode].street, NB_CHARACTER);

			inputInterger(&pBase[indEmptyNode].postalCode, "POSTAL CODE:");

			printf("TOWN: ");
			readString(pBase[indEmptyNode].town, NB_CHARACTER);

			printf("Contact is add successfully\n");

			// update database
			saveContactsInDatabase(pBase);
		}
	}

}

/* edit a contact */
void editContact(CONTACT_INFO_t *const pBase)
{
	int contactToEdit = 0;
	int isExist = 0;
	int index = 0;

	inputInterger(&contactToEdit, "Enter the id of the contact to be edited:");

	isContactExist(pBase, contactToEdit, &isExist, &index);

	if(!isExist) // if contact does not exist
	{
		printf("\033[1;31m"); //Set the text to the color red and bold 
		printf("ERROR: Sorry, this contact is not present.\n"); 
		printf("\033[0m");    //Resets the text to default color
	}
	else
	{
		// editing the contact 
		printf("You are about to edit contact name : %s\n", pBase[index].name);

		printf("Enter new information related to this contact\n");

		inputInterger(&pBase[index].contactId, "ID:");

		printf("NAME: ");
		readString(pBase[index].name, NB_CHARACTER);

		printf("PHONE: ");
		readString(pBase[index].phone, NB_CHARACTER);

		printf("EMAIL: ");
		readString(pBase[index].email, NB_CHARACTER);

		printf("STREET: ");
		readString(pBase[index].street, NB_CHARACTER);

		inputInterger(&pBase[index].postalCode, "POSTAL CODE:");

		printf("TOWN: ");
		readString(pBase[index].town, NB_CHARACTER);

		printf("Contact is edit successfully\n");

		// update database
		saveContactsInDatabase(pBase);
	}
}


/* delete one given contact */
void deleteAContact(CONTACT_INFO_t *const pBase)
{
	int inputContactID = 0;
	int isExist = 0;
	int index = 0;

	inputInterger(&inputContactID, "Enter the id of the contact to be deleted:");

	isContactExist(pBase, inputContactID, &isExist, &index);

	if(!isExist) // if contact does not exist
	{
		printf("\033[1;31m"); //Set the text to the color red and bold 
		printf("ERROR: Sorry, this contact is not present.\n"); 
		printf("\033[0m");    //Resets the text to default color
	}
	else
	{
		// delete the contact to be deleted 
		pBase[index].contactId 			= 0; 
		strcpy(pBase[index].name,		"");
		strcpy(pBase[index].phone,		"");
		strcpy(pBase[index].email,		"");
		strcpy(pBase[index].street,	    "");
		pBase[index].postalCode 		= 0;
		strcpy(pBase[index].town,	    "");

		system("printf \"Deleting contact in %s second(s) | \" \"2\" ");
		loadingBar(2);
		printf("The contact is deleted successfully\n");

		// update database
		saveContactsInDatabase(pBase);
	}

}

/* delete all contacts */
void deleteAllContact(CONTACT_INFO_t *const pBase)
{
	char option[NB_CHARACTER];
	printf("You are about to delete all contacts\n");
	printf("\033[1m"); //set the text in bold
	printf("Are you sure ? (yes (y) or no (n)): ");
	printf("\033[0m"); //Resets the text to default
	scanf("%s", option);
	flushBuffer();

	if((strcmp(option, "yes") == 0) || (strcmp(option, "y") == 0))
	{
		initialization(pBase);
		system("printf \"Deleting contacts in %s second(s) | \" \"5\" ");
		loadingBar(5);
		printf("All contacts deleted successfully\n");
		system("rm -f database.txt"); // delete the database (note it will be create in the main function)
		clearScreen();
	}
	else 
		if((strcmp(option, "no") == 0) || (strcmp(option, "n") == 0))
		{
			clearScreen();
		}
	else
	{
		printf("\033[1;31m"); //Set the text to the color red
		printf("ERROR : Invalid Enter\n"); 
		printf("\033[0m");    //Resets the text to default color
		//flushBuffer();
		printf("Press enter key to clean the screen...");
		getchar();
		system("clear");
	}
}


#endif 
