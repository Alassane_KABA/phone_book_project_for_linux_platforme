#include "userprog.h"

/* global contacts */
CONTACT_INFO_t contacts[MAX_CONTACT];

int main()
{
	int option;

	//initialization(contacts);

	system("touch database.txt"); // create database (if exisit, keep the contain)

	loadContactsFromDatabase(contacts); // load contacts array from the database

	do{
		PROGRAM_NAME;
		menu();

		option = -1;  // option will take -1 in cases of invalid input from inputOption 

		inputOption(&option);

		switch(option)
		{
			case 0: // exit app
				exitApp();
				break;

			case 1: // display all contact 
				displayPhoneBook(contacts);
				getchar();
				clearScreen();
				break;

			case 2: // add a new contact 
				addNewContact(contacts);
				clearScreen();
				break;

			case 3: // edit a contact 
				editContact(contacts);
				clearScreen();
				break;

			case 4: // delete a contact 
				deleteAContact(contacts);
				clearScreen();
				break; 

			case 5: // delete all contact 
				deleteAllContact(contacts);
				//getchar();
				//clearScreen();
				break;

			default:
				defaultEnterOptionCase();
				break;
		}

	}while(option != 0);

	return 0;
}

