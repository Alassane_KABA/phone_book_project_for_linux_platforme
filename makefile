CFLAGS = -g -Wall

TARGET = userprog

all: $(TARGET)

$(TARGET): $(TARGET).c
	${CC} $(CFLAGS) ${LDFLAGS} -o $(TARGET) $(TARGET).c

clean:
	rm -f $(TARGET)